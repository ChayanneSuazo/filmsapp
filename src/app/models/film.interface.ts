export interface FilmI {
    id?: string;
    name: string;
    image: string;
}