import { FoodI } from "./food.interface"; 
import { DocumentReference } from '@angular/fire/firestore';


export interface OrderI {
    id?: string;
    date: Date,
    ticket: number;
    details: Array<FoodI>;
    state: number;
    cinemaListings?: DocumentReference;
    cinemaListingsId: string;
    paidMethod: string;
    amountTotal: number;
    email: string;
}