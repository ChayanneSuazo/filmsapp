import { DocumentReference } from '@angular/fire/firestore';

export interface CinemaListingsI {
    id?: string;
    price: number;
    start: string;
    end: string;
    filmId: string;
    film: DocumentReference;
}