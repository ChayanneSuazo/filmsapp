export interface FoodI {
    id?: string;
    description: string;
    price: number;
    type: string;
}