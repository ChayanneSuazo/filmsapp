import { Injectable } from '@angular/core';

import {
  AngularFirestore,
  AngularFirestoreCollection,
} from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FilmI } from '../models/film.interface';

@Injectable({
  providedIn: 'root',
})
export class FilmService {
  private filmCollection: AngularFirestoreCollection<FilmI>;
  private films: Observable<FilmI[]>;

  constructor(db: AngularFirestore) {
    this.filmCollection = db.collection<FilmI>('films');

    this.films = this.filmCollection.snapshotChanges().pipe(
      map((actions) => {
        return actions.map((f) => {
          const data = f.payload.doc.data();
          const id = f.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  getFilms() {
    return this.films;
  }
}
