import { Injectable } from '@angular/core';
import { UserI } from '../models/user.interface';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private url = 'https://identitytoolkit.googleapis.com/v1';
  private apiKey = 'AIzaSyC7SlAb7OmJ0n9tzqbth6FlGB5VwrL27Ks';
  private userToken: string;
  private email: string;

  constructor(private http: HttpClient) {
    this.readToken();
    this.readEmail();
  }

  login(user: UserI) {
    const authData = {
      ...user, // el uso del ... dice que llame y envie todas las propiedades del objeto usuario
      returnSecureToken: true,
    };
    return this.http
      .post(
        `${this.url}/accounts:signInWithPassword?key=${this.apiKey}`,
        authData // debe recibir 2 parametros el url+key y elauthData
      )
      .pipe(
        map((resp) => {
          console.log('Entro en el mapa de RXJS');
          this.saveToken(resp['idToken']);
          this.saveEmail(resp['email']);
          return resp;
        })
      );
  }

  addUser(user: UserI) {
    const authData = {
      ...user, // el uso del ... dice que llame y envie todas las propiedades del objeto usuario
      returnSecureToken: true,
    };
    return this.http
      .post(
        `${this.url}/accounts:signUp?key=${this.apiKey}`,
        authData // debe recibir 2 parametros el url+key y elauthData
      )
      .pipe(
        map((resp) => {
          console.log('Entroen el mapa de RXJS');
          this.saveToken(resp['idToken']);
          this.saveEmail(resp['email']);
          return resp;
        })
      );
  }

  private saveToken(idToken: string) {
    this.userToken = idToken;
    localStorage.setItem('token', idToken);
  }

  private saveEmail(email: string) {
    this.email = email;
    localStorage.setItem('email', email);
  }

  private readEmail() {
    if (localStorage.getItem('email')) {
      this.email = localStorage.getItem('email');
    } else {
      this.email = '';
    }
  }

  readToken() {
    if (localStorage.getItem('token')) {
      this.userToken = localStorage.getItem('token');
    } else {
      this.userToken = '';
    }
  }

  isAuthenticated(): boolean {
    return this.userToken.length > 2;
  }
}
