import { Injectable } from '@angular/core';

import {
  AngularFirestore,
  AngularFirestoreCollection,
} from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FoodI } from '../models/food.interface';

@Injectable({
  providedIn: 'root',
})
export class FoodService {
  private foodCollection: AngularFirestoreCollection<FoodI>;
  private food: Observable<FoodI[]>;

  constructor(db: AngularFirestore) {
    this.foodCollection = db.collection<FoodI>('food');
    this.food = this.foodCollection.snapshotChanges().pipe(
      map((actions) => {
        return actions.map((f) => {
          const data = f.payload.doc.data();
          const id = f.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  getFood() {
    return this.food;
  }
}
