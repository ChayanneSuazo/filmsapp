import { Injectable } from '@angular/core';

import {
  AngularFirestore,
  AngularFirestoreCollection,
} from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CinemaListingsI } from '../models/cinema_listings.interface';

@Injectable({
  providedIn: 'root'
})
export class CinemaListingsService {

  private cinemaListingsCollection: AngularFirestoreCollection<CinemaListingsI>;
  private cinemaListings: Observable<CinemaListingsI[]>;

  constructor(db: AngularFirestore) {
    this.cinemaListingsCollection = db.collection<CinemaListingsI>("cinemaListings");
    this.cinemaListings = this.cinemaListingsCollection.snapshotChanges().pipe(
      map((actions) => {
        return actions.map((c) => {
          const data = c.payload.doc.data();
          const id = c.payload.doc.id;
          return {id, ...data};
        });
      })
    );
   }

  getCinemaListings() {
    return this.cinemaListings;
  }
}
