import { Injectable } from '@angular/core';

import {
  AngularFirestore,
  AngularFirestoreCollection,
} from '@angular/fire/firestore';
import { OrderI } from '../models/order.interface';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class OrderService {
  private orderCollection: AngularFirestoreCollection<OrderI>;
  private orders: Observable<OrderI[]>;

  constructor(db: AngularFirestore) {
    this.orderCollection = db.collection<OrderI>('orders');
    this.orders = this.orderCollection.snapshotChanges().pipe(
      map((actions) => {
        return actions.map((res) => {
          const data = res.payload.doc.data();
          const id = res.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  getOrders() {
    return this.orders;
  }

  addOrder(order) {
    let result: boolean;
    this.orderCollection
      .add(order)
      .then(() => {
        result = true;
      })
      .catch(() => {
        result = false;
      });
    return result;
  }

  deleteOrder(orderId: string): Promise<void> {
  //   let today = new Date();
  //   let date = new Date(order.date.seconds * 1000);
  //   let result: boolean;
  //   this.orderCollection
  //     .doc(order.id)
  //     .update({
  //       email: order.email,
  //       details: order.details,
  //       state: order.state,
  //       date: date,
  //       cinemaListingsId: order.cinemaListingId,
  //       paidMethod: order.paidMethod,
  //       amountTotal: order.amountTotal,
  //       ticket: order.ticket,
  //     })
  //     .then(() => {
  //       result = true;
  //     })
  //     .catch(() => {
  //       result = false;
  //     });

  //   return result;

  return new Promise( async (resolve, reject) => {
    try {
      const result = await this.orderCollection.doc(orderId).delete();
      resolve(result);
    } catch (error) {
      reject(error.message);
    }
  } )
  }
}
