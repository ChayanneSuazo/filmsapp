import { Component, Input, OnInit } from '@angular/core';
import { FoodI } from '../../models/food.interface';
import { CinemaListingsI } from '../../models/cinema_listings.interface';
import { FilmI } from '../../models/film.interface';
import { FoodService } from '../../services/food.service';
import { FilmService } from '../../services/film.service';
import { OrderService } from '../../services/order.service';
import { ActionSheetController, ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { CinemaListingsService } from '../../services/cinema-listings.service';
import { OrderI } from '../../models/order.interface';

@Component({
  selector: 'app-order',
  templateUrl: './order.page.html',
  styleUrls: ['./order.page.scss'],
})
export class OrderPage implements OnInit {
  tickets: number;
  movieSelected: string;
  paidMethod: string;
  messageButton: string;
  isPaidMethod: boolean;
  @Input() email: string;
  @Input() order: string;
  @Input() state: number;

  foodStorage: Array<any> = new Array<any>(); 
  food: FoodI[];
  totalTickets: Array<any> = new Array<any>();
  cinemaListings: CinemaListingsI[];
  films: FilmI[];
  methods: Array<string> = ['Tarjeta', 'Efectivo'];
  orders: OrderI[];
  amountTotal: number;

  constructor(
    private foodService: FoodService,
    private filmService: FilmService,
    private cinemaListingsService: CinemaListingsService,
    private orderService: OrderService,
    private modalController: ModalController,
    private actionSheetController: ActionSheetController,
    private router: Router
  ) {}

  ngOnInit() {
    this.isPaidMethod = true;
    this.tickets = 1;

    this.uploadLocalStorageData();
    this.uploadFirebaseData();

    let n = 0;
    while (n < 30) {
      n++;
      this.totalTickets.push(n);
    }
  }

  //  DATA
  uploadFirebaseData() {
    this.cinemaListingsService
      .getCinemaListings()
      .subscribe((res) => (this.cinemaListings = res));
    this.filmService.getFilms().subscribe((res) => (this.films = res));
    this.foodService.getFood().subscribe((res) => (this.food = res));
    this.orderService.getOrders().subscribe((res) => (this.orders = res));
  }

  uploadLocalStorageData(): void {
    if (this.state === 1) {
      if (localStorage.getItem('movieSelected')) {
        this.movieSelected = localStorage.getItem('movieSelected');
      }

      if (localStorage.getItem('foodStorage')) {
        this.foodStorage = JSON.parse(localStorage.getItem('foodStorage'));
      }
    } else if (this.state === 2) {
      this.orderProcessed();
    }

    if (localStorage.getItem('email')) {
      this.email = localStorage.getItem('email');
    }

    if (this.state === 1) {
      if (this.email) {
        this.messageButton = 'Realizar Pago';
      } else {
        this.messageButton = 'Accede para confirmar tu orden';
      }
    } else if (this.state === 2) {
      this.messageButton = 'Cancelar Ticket';
    }
  }

  //  Process

  orderProcessed() {
    let order: any;
    order = this.order;
    this.movieSelected = order.cinemaListingsId;
    this.foodStorage = order.details;
    this.tickets = order.ticket;
    this.isPaidMethod = order.paidMethod;

    return order;
  }

  amountTotalFood() {
    let total = 0;
    this.foodStorage.forEach((f) => {
      total += f.price * f.quantity;
    });
    return total;
  }

  getAmountTotal(cinemaListingsPrice, tickets) {
    const amountTotalFood = this.amountTotalFood();
    this.amountTotal = cinemaListingsPrice * tickets + amountTotalFood;

    return this.amountTotal;
  }

  tenerFuncion() {
    let evento: Array<any>;
    this.cinemaListings.forEach((c) => {
      evento.push(c);
    });
    return evento;
  }

  async login() {
    // this.modalController.dismiss();
    // const modal = await this.modalController.create({
    //   component: LoginPage,
    //   componentProps: {
    //   }
    // });
    // await modal.present();
  }

  processOrder(cinemaListings: CinemaListingsI) {
    console.log(cinemaListings);
    let today = new Date();
    let orderObject: OrderI;
    let order: any = this.order;

    if (this.state === 1) {
      if (this.email) {
        if (this.paidMethod) {
          orderObject = {
            email: this.email,
            details: this.foodStorage,
            state: 1,
            date: today,
            cinemaListingsId: cinemaListings.id,
            amountTotal: this.getAmountTotal(
              cinemaListings.price,
              this.tickets
            ),
            ticket: this.tickets,
            paidMethod: this.paidMethod,
          };
          this.isPaidMethod = true;

          if (!this.orderService.addOrder(orderObject)) {
            this.orders.push(orderObject);
            localStorage.setItem('orders', JSON.stringify(this.orders));
            this.created();
            localStorage.removeItem('foodStorage');
            this.modalController.dismiss();
            this.router.navigate(['/tabs/home']);
          } else {
            this.notCreated();
          }
        } else {
          this.isPaidMethod = false;
        }
      }
      // else {
      //   this.login();
      // }
    }
    if (this.state === 2) {
      orderObject = {
        email: order.email,
        details: order.details,
        state: 0,
        date: order.date,
        cinemaListingsId: order.cinemaListingsId,
        paidMethod: order.paidMethod,
        amountTotal: order.amountTotal,
        ticket: order.ticket,
        id: order.id,
      };

      if (this.deleteTicket(order.id)) {
        this.deleted();

        this.onRemoveStorage(order.cinemaListingsId);
        this.router.navigate(['/tabs/home']);
      } else {
        this.notCreated();
      }
    }
  }

  onRemoveStorage(cinemaListingsId) {
    const ordersStorage = JSON.parse(localStorage.getItem('orders'));
    const place = ordersStorage.indexOf(cinemaListingsId);

    if (place > -1) {
      localStorage.removeItem('orders');
      ordersStorage.splice(place, 1);
      localStorage.setItem('orders', JSON.stringify(ordersStorage));
    }
  }

  async deleteTicket(orderId) {
    try {
      await this.orderService.deleteOrder(orderId);
      return true;
    } catch (error) {
      return false;
    }
  }

  async created() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Registrado',
      buttons: [
        {
          text: 'Su orden fue registrada con exito',
          icon: 'checkmark-done',
        },
      ],
    });
    await actionSheet.present();
    setTimeout(() => {
      this.actionSheetController.dismiss();
    }, 1500);
    location.reload();
  }

  async deleted() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Cancelado',
      buttons: [
        {
          text: 'Su orden fue cancelada con exito',
          icon: 'checkmark-done',
        },
      ],
    });
    await actionSheet.present();
    setTimeout(() => {
      this.actionSheetController.dismiss();
      this.modalController.dismiss();
    }, 1500);
    location.reload();
  }

  async notCreated() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Error',
      buttons: [
        {
          text: 'Su orden no pudo ser procesada',
          icon: 'alert',
        },
      ],
    });
    await actionSheet.present();
    setTimeout(() => {
      this.actionSheetController.dismiss();
    }, 1500);
  }

  closeOrder() {
    this.modalController.dismiss();
  }
}
