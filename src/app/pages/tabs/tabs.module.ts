import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TabsPageRoutingModule } from './tabs-routing.module';

import { TabsPage } from './tabs.page';
import { FoodPageModule } from '../food/food.module';
import { OrderPageModule } from '../order/order.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TabsPageRoutingModule,
    FoodPageModule,
    OrderPageModule
  ],
  declarations: [TabsPage]
})
export class TabsPageModule {}
