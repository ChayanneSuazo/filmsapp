import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UserI } from '../../models/user.interface';
import { AuthService } from '../../services/auth.service';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  user: UserI = new UserI();

  constructor(private authService: AuthService, private toastController: ToastController) {}

  ngOnInit() {}

  login(form: NgForm) {
    if (form.invalid) {
      return;
    }

    this.authService.login(this.user).subscribe(
      (res) => {

      },
      (err) => {
        this.errorLogin();
      }
    );
  }

  async errorLogin() {
    const toast = await this.toastController.create({
      header: 'Error',
      message: 'Usuario o Contraseña inválidos',
      duration: 1500,
    });
    toast.present();
  }

  async correctLogin() {
    const toast = await this.toastController.create({
      header: 'Login Correcto',
      message: 'Login realizado con exito',
      duration: 1500,
    });
    toast.present();
  }
}
