import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';

// Interfaces

import { FilmI } from '../../models/film.interface';
import { CinemaListingsI } from '../../models/cinema_listings.interface';

// Services
import { FilmService } from '../../services/film.service';
import { CinemaListingsService } from '../../services/cinema-listings.service';

// Pages
import { FoodPage } from '../food/food.page';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  films: FilmI[];
  cinemaListings: CinemaListingsI[];

  constructor(
    private filmService: FilmService,
    private cinemaListingsService: CinemaListingsService,
    private modalController: ModalController,
    private toastController: ToastController
  ) {}

  ngOnInit() {
    this.cinemaListingsService
      .getCinemaListings()
      .subscribe((res) => (this.cinemaListings = res));
    this.filmService.getFilms().subscribe((res) => (this.films = res));
  }

  async showFood(type: string) {
    const modal = await this.modalController.create({
      component: FoodPage,
      cssClass: 'shortModal',
      componentProps: {
        foodType: type,
      },
    });

    await modal.present();
  }

  selectCinemaListings(cinemaListings) {
    if (localStorage.getItem('movieSelected')) {
      localStorage.removeItem('movieSelected');
    }
    localStorage.setItem('movieSelected', cinemaListings.id);
    this.showMessage(cinemaListings.start);
  }

  async showMessage(cinemaHour) {
    const toast = await this.toastController.create({
      header: 'Funcion Seleccionada',
      message: 'Ha seleccionado una funcion a las ' + cinemaHour,
      duration: 1500,
    });
    toast.present();
  }
}
