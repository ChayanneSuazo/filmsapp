import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import {
  ModalController,
  ActionSheetController,
  ToastController,
} from '@ionic/angular';

// Interface
import { FoodI } from '../../models/food.interface';

// Services
import { FoodService } from '../../services/food.service';
import { Observable, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { OrderPage } from '../order/order.page';

@Component({
  selector: 'app-food',
  templateUrl: './food.page.html',
  styleUrls: ['./food.page.scss'],
})
export class FoodPage implements OnInit, OnDestroy {
  quantity: number;
  price: number;
  food: FoodI[];
  foodStorage: Array<any> = new Array<any>();
  foodShow: Array<any> = new Array<any>();
  foodSelected: string;
  foodType$: Subscription = this.foodService.getFood().subscribe(  (res) => {
    this.food = res;
    this.separateFood();
  });

  @Input() foodType: string;

  constructor(
    private foodService: FoodService,
    private modalController: ModalController,
    private actionSheetController: ActionSheetController,
    private toastController: ToastController
  ) {}

  ngOnInit() {
    console.log(this.foodType);
    if (localStorage.getItem('foodStorage')) {
      this.foodStorage = JSON.parse(localStorage.getItem('foodStorage'));
    }

    
  }

  ngOnDestroy() {
    this.foodType$.unsubscribe();
  }

  addFood() {
    this.food.forEach((f) => {
      if (f.id === this.foodSelected) {
        this.price = f.price;
      }
    });
    if (this.foodSelected && this.quantity) {
      this.foodStorage.push({
        id: this.formId(),
        foodId: this.foodSelected,
        quantity: this.quantity,
        price: this.price,
      });
    }
    localStorage.setItem('foodStorage', JSON.stringify(this.foodStorage));
  }

  formId() {
    const fecha = new Date();
    let mili = fecha.getMilliseconds().toString();
    const rand = Math.floor(Math.random() * 999).toString();
    fecha.getMilliseconds();
    switch (mili.length) {
      case 2:
        mili += Math.floor(Math.random() * 9).toString();
        break;
      case 1:
        mili += Math.floor(Math.random() * 99).toString();
        break;
    }

    switch (rand.length) {
      case 2:
        mili += Math.floor(Math.random() * 9).toString();
        break;
      case 1:
        mili += Math.floor(Math.random() * 99).toString();
        break;
    }

    const abc = 'abcdefghijklmnopqrstuvwxyz';
    let letras = '';
    let intento = 0;
    do {
      intento++;
      letras += abc[Math.floor(Math.random() * 25)];
    } while (intento < 5);
    return mili + rand + letras;
  }

  closeModal() {
    this.modalController.dismiss();
  }
  deleteFood(foodId) {
    const place = this.foodStorage.indexOf(foodId);
    if (place > -1) {
      localStorage.removeItem('foodStorage');
      this.foodStorage.splice(place, 1);
      localStorage.setItem('foodStorage', JSON.stringify(this.foodStorage));
    }
  }
  async processOrder() {
    this.closeModal();
    if (localStorage.getItem('movieSelected')) {
      const modal = await this.modalController.create({
        component: OrderPage,
        componentProps: {
          state: 1,
        },
      });
      await modal.present();
    } else {
      this.errorProcess();
    }
  }

  async errorProcess() {
    const toast = await this.toastController.create({
      header: 'Error',
      message: 'Debe elegir una funcion para procesar su orden.',
      duration: 1500,
    });
    toast.present();
  }

  separateFood() {
    if (this.foodType != '') {
      switch (this.foodType) {
        case 'beverages': 
          // this.foodShow = this.food.filter((food) => {
          //   console.log(food);
          //   return food.type === this.foodType;
          // });
          this.food.forEach((f) => {
            if (f.type == this.foodType) {
              this.foodShow.push(f);
            }
          });
        break;
        case 'snacks': 
          // this.foodShow = this.food.filter((food) => {
          //   return food.type === this.foodType;
          // });
          this.food.forEach((f) => {
            if (f.type == this.foodType) {
              this.foodShow.push(f);
            }
          });
        break;
      }
    }
  }
}
