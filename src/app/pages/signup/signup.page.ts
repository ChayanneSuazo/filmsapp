import { Component, OnInit } from '@angular/core';
import { UserI } from '../../models/user.interface';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
  user: UserI;

  constructor(private authService: AuthService) {}

  ngOnInit() {
    this.user = new UserI();
  }

  onSubmit(form: NgForm) {
    if (form.invalid) {
      return;
    }

    this.authService.addUser(this.user).subscribe(
      (res) => {
        console.log(res);
      },
      (err) => {
        console.error(err.error.error.message);
      }
    );
    /*console.log('formulario enviado');
    console.log(this.usuario);
    console.log(form);*/
  }
}
