import { Component, OnDestroy, OnInit } from '@angular/core';
import { OrderService } from '../../services/order.service';
import { FilmService } from '../../services/film.service';
import { CinemaListingsService } from '../../services/cinema-listings.service';
import { ModalController } from '@ionic/angular';
import { FilmI } from '../../models/film.interface';
import { CinemaListingsI } from 'src/app/models/cinema_listings.interface';
import { OrderI } from '../../models/order.interface';
import { OrderPage } from '../order/order.page';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.page.html',
  styleUrls: ['./ticket.page.scss'],
})
export class TicketPage implements OnInit, OnDestroy {
  email: string;
  localOrders: Array<any> = new Array<any>();
  films: FilmI[];
  cinemaListings: CinemaListingsI[];
  orders: OrderI[];
  orders$: Subscription = this.orderService.getOrders().subscribe((res) => {
    this.orders = res;
    this.viewOrders();
  });
  constructor(
    private orderService: OrderService,
    private filmService: FilmService,
    private cinemaListingsService: CinemaListingsService,
    private modalController: ModalController
  ) {}

  ngOnInit() {
    this.email = localStorage.getItem('email');

    this.uploadFirebaseData();
  }

  ngOnDestroy() {
    this.orders$.unsubscribe();
  }

  uploadFirebaseData() {
    this.cinemaListingsService
      .getCinemaListings()
      .subscribe((res) => (this.cinemaListings = res));
    this.filmService.getFilms().subscribe((res) => (this.films = res));
    
  }

  async viewOrder(orderObject) {
    console.log(orderObject);
    if (localStorage.getItem('movieSelected')) {
      const modal = await this.modalController.create({
        component: OrderPage,
        componentProps: {
          order: orderObject,
          state: 2,
        },
      });
      await modal.present();
    }
  }

  viewOrders() {
    this.orders.forEach((order) => {
      if (order.email === this.email && order.state === 1) {
        this.cinemaListings.forEach((cinema) => {
          if (cinema.id === order.cinemaListingsId) {
            this.films.forEach((film) => {
              if (film.id === cinema.filmId) {
                this.localOrders.push({
                  filmName: film.name,
                  schedule: cinema.start + ' - ' + cinema.end,
                  ticket: order.ticket,
                  id: order.id,
                  orderObject: order,
                });
              }
            });
          }
        });
      }
    });
    return this.localOrders;
  }
}
